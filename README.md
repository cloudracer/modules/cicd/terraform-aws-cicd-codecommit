# Terraform CI/CD CodeCommit

## Overview

This Module creates a CodeCommit Repository

## Usage

````
module "cicd_codecommit" {
  source  = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-cicd-codecommit.git?ref=master"

  repository_name = "${var.organisation}-landingzone-codecommit-tflz"
  repository_description = "CodeCommit Repository for the AWS Terraform Landingzone"

  tags            = local.tags
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_codecommit_repository.repo](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codecommit_repository) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_repository_description"></a> [repository\_description](#input\_repository\_description) | CodeBuild git repository description | `string` | n/a | yes |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | CodeBuild git repository name | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that should be applied to all Resources | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_terraform_codecommit_repo_arn"></a> [terraform\_codecommit\_repo\_arn](#output\_terraform\_codecommit\_repo\_arn) | n/a |
| <a name="output_terraform_codecommit_repo_name"></a> [terraform\_codecommit\_repo\_name](#output\_terraform\_codecommit\_repo\_name) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
